import { Component } from 'react';
import { Contents } from './Contents';
import './App.css';

class App extends Component {
  render() {
    const html = this.header();
    return html;
  }

  header() {
    return (
      <div className="App">
        <header className="App-header">
          <Contents />
        </header>
      </div>
    );
  }
}

export default App;

