import { Entity } from './Entity';
import Ground from './Ground';

export class Player extends Entity {
    constructor(props) {
        super(props);
        this.state.className = "Player";
        this.state.speed = 2;
        this.force = 0;
    }

    move(speed) {
        const keyCode = this.state.keyCode;
        const left = keyCode === "A" ? -1 : 0;
        const right = keyCode === "D" ? 1 : 0;

        this.position.x += (left + right) * speed;
    }

    onGravity() {
        const ground = Entity.list[Ground.name][0];
        const groundY = ground.getRect().top - this.getRect().height;
        const gravity = 0.05;
        if (this.position.y >= groundY) {
            this.position.y = groundY;
            return;
        }

        this.force += gravity;
        this.position.y += this.force;
    }

    onUpdate() {
        this.setFocus();
        this.onGravity();
        this.move(this.state.speed);
    }

    componentDidMount() {
        super.componentDidMount();
    }
    onKeyPress(key) {
        this.state.keyCode = key.code.charAt(key.code.length - 1);
    }

    render() {
        this.attribute = {
            tabIndex: 0,
            onKeyPress: this.onKeyPress.bind(this),
            onKeyUp: key => { this.state.keyCode = null; }
        };
        return super.render();
    }
}

export default Player;