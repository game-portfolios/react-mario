import { Component, createRef } from 'react';

export class Entity extends Component {
    static list = [];

    constructor(props) {
        super(props);
        this.selector = createRef();
        this.state = { className: "none" };
        this.contents = null;
        this.attribute = {};

        if (!Entity.list.includes(this.constructor.name)) {
            Entity.list[this.constructor.name] = [];
        }
        console.log(this.constructor.name);
        Entity.list[this.constructor.name].push(this);
    }

    postionUpdate() {
        const target = this.selector.current;
        if (target == null) {
            return;
        }
        const style = target.style;
        this.position = {
            _x: 0, _y: 0,
            get x() {
                return this._x;
            },
            set x(value) {
                this._x = value;
                style.left = this._x + "px";
            },
            get y() {
                return this._y;
            },
            set y(value) {
                this._y = value;
                style.top = this._y + "px";
            },
        };
        return;
    }

    getRect() {
        const target = this.selector.current;
        if (target == null) {
            return;
        }
        return target.getBoundingClientRect();
    }

    setFocus() {
        const target = this.selector.current;
        if (target == null) {
            return;
        }
        target.focus();
    }

    onUpdate() { }

    componentDidMount() {
        this.postionUpdate();
        const update = () => {
            this.onUpdate();
        };
        setInterval(update, 10);
    }

    componentDidUpdate() {
        this.postionUpdate();
    }

    render() {
        return (
            <div className={this.state.className} ref={this.selector} {...this.attribute}>
                {this.contents}
            </div>
        );
    }
}
