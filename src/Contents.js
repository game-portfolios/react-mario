import { Component, createRef } from 'react';
import { Ground } from './Ground';
import { Player } from './Player';

export class Contents extends Component {
    constructor(props) {
        super(props);
        this.ref = createRef();
    }

    render() {
        const result = (
            <>
                <Ground />
                <Player />
            </>
        );
        return result;
    }
}

export default Contents;