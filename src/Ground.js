import { Entity } from './Entity';

export class Ground extends Entity {
    constructor(props) {
        super(props);
        this.state.className = "Ground";
    }

    htmlDivs() {
        const divArray = ["div1", "div2"];
        const getMapFunc = item =>
            < div key={item.toString()} className={item} ></div>;
        this.contents = divArray.map(
            getMapFunc
        );
        //console.log(this.contents);
    }

    render() {
        this.htmlDivs();
        return super.render();
    }
}

export default Ground;